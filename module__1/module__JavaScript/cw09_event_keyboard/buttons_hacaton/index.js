const allButtons = document.querySelectorAll('.btn')

window.addEventListener('keyup', function (event) {
	allButtons.forEach(function (singleButton) {
		if (event.key === singleButton.id) {
			singleButton.classList.add('active')
		} else {
			singleButton.classList.remove('active')
		}
	})
})

// element.classList.add('active')
// element.classList.remove('active')
