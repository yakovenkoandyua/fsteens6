// keyup
// keydown
// keypress

const pacman = document.querySelector('.pacman')

let axisY = 0 // вертикаль
let axisX = 0 // горизонталь

window.addEventListener('keypress', function (event) {
	// if (event.key === 'w') {
	// 	console.log('up')
	// } else if (event.key === 's') {
	// 	console.log('down')
	// } else if (event.key === 'd') {
	// 	console.log('right')
	// } else if (event.key === 'a') {
	// 	console.log('left')
	// }

	switch (event.key) {
		case 'w':
			axisY -= 20
			pacman.style.top = axisY + 'px'
			pacman.style.transform = 'rotate(-90deg)'
			break
		case 's':
			axisY += 20
			pacman.style.transform = 'rotate(90deg)'
			pacman.style.top = axisY + 'px'
			break
		case 'd':
			axisX += 20
			pacman.style.transform = 'rotate(0)'
			pacman.style.left = axisX + 'px'
			break
		case 'a':
			axisX -= 20
			pacman.style.transform = 'rotateY(180deg)'
			pacman.style.left = axisX + 'px'
			break
	}
})
