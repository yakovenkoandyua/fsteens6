/* ЗАДАЧА - 1
 * Создать переменую, записать в нее число, вывести значение этой переменной в консоль
 * */
// let number = 322

// console.log(number)

/* ЗАДАНИЕ 2
 * Создать переменную при помощи ключевого слова const, записать в нее число.
 * Попробовать переприсвоить этой переменной новое число.
 * */

// const number = 222

// number = 1
// console.log("🚀 ==== > number", number);




/* ЗАДАНИЕ 3
 * Записать в переменную '123', вывести в консоль typeof этой переменной.
 * Преобразовать эту переменную в численный тип при помощи унарный плюс +
 * После этого повторно вывести в консоль typeof этой переменной.
 * */

// let temp = '123'


// console.log(typeof +temp);

// console.log(temp);
// console.log(+temp);




/* ЗАДАНИЕ 4
 * Вывести на экран уведомление с текстом "Hello! This is alert" при помощи модального окна alert
 * */

// alert('hello ! this is alert')

/* ЗАДАНИЕ 5
 * Вывести на экран модальное окно prompt с сообщением "Enter the number".
 * Результат выполнения модального окна записать в переменную, значение которой вывести в консоль.
 * */

// let answer = prompt('Enter the number')

// console.log(answer );


/* ЗАДАНИЕ 6
 * При помощи модального окна prompt получить от пользователя два числа.
 * Вывести в консоль сумму, разницу, умножение, результат деления и остаток от деления их друг на друга.
 * */

let firstNumber = +prompt('enter first number')
let secondNumber = +prompt('enter second number')

console.log(firstNumber + secondNumber)
console.log(firstNumber - secondNumber)
console.log(firstNumber * secondNumber)
console.log(firstNumber / secondNumber)



/* ЗАДАНИЕ 7
 * Спросить у пользователя какого он года рождения, и посчитать сколько ему лет*/



