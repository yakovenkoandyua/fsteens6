function createLetterBlock(word) {
	const parent = document.querySelector('.word')

	for (let i = 0; i < word.length; i++) {
		const wordItem = document.createElement('div')
		wordItem.className = 'word-item'

		const wordLetter = document.createElement('p')
		wordLetter.className = 'word-item-text'

		wordLetter.textContent = word[i]

		wordItem.append(wordLetter)

		parent.append(wordItem)
	}
}

const lineLeft = document.querySelector('.line-left')
const lineTop = document.querySelector('.line-top')
const lineBot = document.querySelector('.line-bot')
const hangmanHandLeft = document.querySelector('.hangman-hand-left')
const hangmanHandRight = document.querySelector('.hangman-hand-right')
const hangmanFootLeft = document.querySelector('.hangman-foot-left')
const hangmanFootRight = document.querySelector('.hangman-foot-right')
const hangmanHead = document.querySelector('.hangman-head')
const hangmanBody = document.querySelector('.hangman-body')

document.getElementById('letter-sumbit').addEventListener('click', function (event) {
	event.preventDefault()

	const field = document.querySelector('.letter-form-text')

	findLetter(field.value)

	field.value = ''
})

let error = 0

function findLetter(value) {
	const allLetters = [...document.querySelectorAll('.word-item-text')]

	const showLetterWrapper = document.querySelector('.show-letters')
	
	const errorLetter = document.createElement('p')
	
	const check = allLetters.some(i => i.textContent === value)
	
	allLetters.forEach(function (singleLetter) {
		if (value === singleLetter.textContent) {
			singleLetter.classList.add('active')	
		} else {

			errorLetter.textContent = value
			errorLetter.className = 'show-letter-item'
			 
			if (!check) showLetterWrapper.append(errorLetter)
		}
	})


	if (!check) {
		switch (error) {
			case 0:
				lineLeft.style.display = 'block'
				break
			case 1:
				lineTop.style.display = 'block'
				break
			case 2:
				lineBot.style.display = 'block'
				break
			case 3:
				hangmanFootLeft.style.display = 'block'
				break
			case 4:
				hangmanFootRight.style.display = 'block'
				break
			case 5:
				hangmanBody.style.display = 'block'
				break
			case 6:
				hangmanHandLeft.style.display = 'block'
				break
			case 7:
				hangmanHandRight.style.display = 'block'
				break
			case 8:
				hangmanHead.style.display = 'block'
				break
		}
		error++
	}
}

;(function riddleWord() {
	const modal = document.querySelector('.modal')
	const formModal = document.querySelector('.modal-form')

	formModal.addEventListener('submit', function (event) {
		event.preventDefault()

		const fieldWord = document.getElementById('word')

		createLetterBlock(fieldWord.value)

		modal.classList.add('active')
	})
})()
