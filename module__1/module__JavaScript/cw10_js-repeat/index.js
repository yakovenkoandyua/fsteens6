// null
// undefined

// const numberSingle = 112
// let name = 'andrey'

// if (3 > 5) {
// 	console.log('work')
// } else if (3 > 2) {
// 	console.log(' work again')
// } else {
//     console.log('not work');
// }

// for (let i = 0; i < 10; i++) {
//     console.log(i);
// }

// let count = 0
// let answer = prompt('enter something?')

// while(answer !== '') {
//      answer = prompt('enter something?')
// }

// function createBlock(size) {
//     const element = document.createElement('div')
//     element.style.cssText = `width: ${size}px; height: ${size}px; background: green`
//     return element
// }

// const div1 = createBlock(100)
// console.log("🚀 ==== > div1", div1);
// const div2 = createBlock(250)

// const school = {
//     director: null,
//     title: 'Школа N-1',
//     firstClass: ['Petrov', 'Sidorov'],
//     secondClass: ['yakovenko', 'sydorenko']
// }

// alert(school.director)

// const someArray = ['111', '222','3333','32432']

// const filterArray = someArray.filter(function(item) { // метод фильтр должен всегда вернуть значение true/false
//     console.log(item);
//     return item.length > 3
// })
// console.log("🚀 ==== > filterArray", filterArray);

// const title = document.createElement('h1')
//     title.textContent = 'hello i"m from JS'

// const container = document.getElementById('root')
// const container = document.querySelector('.root')
// const container = document.querySelectorAll('.root')

// container.append(title)

// before
// after
// append
// prepend

const mailStorage = [
	{
		subject: 'Hello world',
		from: 'gogidoe@somemail.nothing',
		to: 'lolabola@ui.ux',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
	{
		subject: 'How could you?!',
		from: 'ladyboss@somemail.nothing',
		to: 'ingeneer@nomail.here',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
	{
		subject: 'Acces denied',
		from: 'info@cornhub.com',
		to: 'gogidoe@somemail.nothing',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
]

function renderMail(data) { // создаем функицю для отрисовки писем где параметром указываем data
	const emailsWrapper = document.querySelector('.emails')
	// получаем див с хтмл для размещения всех писем
	data.forEach(function (singleMail) { // на массиве со всем обьектами применяем метод массива forEach чтобы получить каждый элемент по отдельности
		const email = document.createElement('div')
		email.className = 'email-item'
		// создаем див для одного письма и задаем ему нужный класс

		email.innerHTML = `
        <h2>${singleMail.subject}</h2>
        <a href=${singleMail.from}>${singleMail.from}</a>
        <a href=${singleMail.to}>${singleMail.from}</a>
        <p hidden>${singleMail.text}</p>
        `
		// наполняем див хтмл разметкой с использованием шаблонной строки(обратные кавычки) для возможности использования переменных внтури строки 
        
        emailsWrapper.append(email)
		// помещяем наш созданный див с контантом письма в разметку хтмл, а конкретно в ранее получиней элемент со страницы
	})

    emailsWrapper.addEventListener('click', function(event) {
			// вешаем обработчик событий на весь родительский элемент для того чтобы отслеживать нажатие по конкретному письму и показать в не скрытый текст
			const item = event.target.closest('.email-item')
			// event.target = элемент по которому мы кликаем
			// event.target.closest() = находим ближайший элемент в котором находиться всё письмо

			const text = item.querySelector('p')
			// item.querySelector = ищем внутри кликнутого элемента текст который нужно отбразить

			text.hidden = !text.hidden
			// назначаем отрицательное самому же себе значения hidden для того чтобы или показать или скрыть текст
			// false = !false
		})
}

renderMail(mailStorage)
