/* ЗАДАНИЕ - 1
 * Написать функцию суммирования.
 * Принимает аргументы: первое число и второе число
 * Возвращаемое функцией значение: сумма двух аргументов*/

// function getSummNumbers(firstNumber, secondNumber) {
// 	if (firstNumber !== undefined && secondNumber !== undefined) {
// 		const result = firstNumber + secondNumber
// 		return result
// 	} else {
// 		console.log('не все параметры заданы!!!');
// 	}
// }

// const summ = getSummNumbers(4, 8)
// console.log(summ)

/* ЗАДАНИЕ - 2
 * Написать функцию которая будет принимать два аргумента - число с которого НАЧАТь отсчет и число до которого нужно досчитать.
 * Под отсчетом имеется в виду последовательный вывод чисел в консоль с увеличением на единицу.
 */

// function showRangeNumbers(start, end) {
// 	if (start > end) {
// 		for (let i = end; i < start; i++) {
// 			document.write(`<li>${i}</li>`)
// 		}
// 	} else {
// 		for (let i = start; i < end; i++) {
// 			document.write(`<li>${i}</li>`)
// 		}
// 	}
// }

// let userFirstNum = +prompt('enter first number')
// let userSecondNum = +prompt('enter second num')

// showRangeNumbers(userFirstNum, userSecondNum)

/* ЗАДАНИЕ - 3
 * Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
 * */

// function summAllArguments() {
// 	// console.log(arguments[3])
// 	let quantityNumber = arguments.length
// 	let result = 0
// 	for (let i = 0; i < quantityNumber; i++) {
// 		// result = result + arguments[i]
// 		result += arguments[i]
// 	}
// 	return result
// }

// const summ = summAllArguments(5,21,3)
// console.log(summ)

/* ЗАДАНИЕ - 4
 * Написать функцию, которая проверяет количество переданных агрументов.
 * Принимаемые аргументы - три числа
 * Если число переданых аргументов не равно трем - выводить сообщение с текстом "Функция вызвана с *КОЛИЧЕСТВО ПЕРЕДАННЫХ АРГУМЕНТОВ* параметрами!"
 * */

function checkArgs(){
	if(arguments.length === 3) {
		console.log('все гуд')
	} else {
		console.log(`Функция вызвана с ${arguments.length} параметрами!`);
	}
}
checkArgs(8,7,7)


/* ЗАДАНИЕ - 5
 * Написать функцию calculateResidue, которая будет выводить в консоль ВСЕ числа в заданом диапазоне, которые кратны 3 аргументу функции.
 * Аргументы функции:
 *   1) число С которого начинается диапазон (включительно)
 *   2) число которым диапазон заканчивается (включительно)
 *   3) число КРАТНЫЕ КОТОРОМУ нужно вывести в консоль
 * Возвращаемое значение - отсутствует
 * В идеале разделить это на 3 фнукциию
 * 1 - спрашивает два числа(диапазон),
 * 2 - спрашивает третье число и выводит в консоль результаты,
 * 3 - сборная солянка из предыдущих двух.
 * */

/* ЗАДАНИЕ - 6
 * Написать функцию getPrice(), которая будет вычислять стоимость бургера в зависимости от того какой у него размер и начинка.
 * Размер и начинку определяет пользователь. УСЛОВНОЕ УПРОЩЕНИЕ - он умеет вводить ТОЛЬКО имена констант, т.е. проверка на некорректность введения данных не требуется.
 *
 * Размеры и начинки записаны в константы:*/
const SIZE_SMALL = {
	name: 'small',
	price: 15,
	cal: 250,
}

const SIZE_LARGE = {
	name: 'large',
	price: 25,
	cal: 340,
}

const STUFFING_CHEASE = {
	name: 'chease',
	price: 4,
	cal: 25,
}

const STUFFING_SALAD = {
	name: 'salad',
	price: 5,
	cal: 5,
}

const STUFFING_BEEF = {
	name: 'beef',
	price: 10,
	cal: 50,
}









