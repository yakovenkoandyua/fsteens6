/* ЗАДАНИЕ - 1
 * Пользователь вводит в модальное окно любое число. Нужно вывести на экран "Четное" или "Не четное" в зависимости от того четное число или нет.
 */

// const userNumber = +prompt('check odd or even number')

// if(userNumber % 2 === 0) {
//     console.log('число четное')
// } else {
//     console.log('не четное число')
// }

/* ЗАДАНИЕ - 2
 * Спросить пользователя на каком языке он хочет увидеть список дней недели. Пользователь может ввести  - ru, eng, ukr
 * Вывести на экран список дней недели на выбранном языке.
 * */

// let langEng = 'monday'
// let langUkr = 'понедiлок'
// let langRu = 'понедельник'

// let langUser = prompt('выберите язык')

// if (langUser === 'eng') {
// 	console.log(langEng)
// } else if (langUser === 'ru') {
// 	console.log(langRu)
// } else if (langUser === 'ukr') {
// 	console.log(langUkr)
// } else {
// 	console.log('ввод не коректный')
// }

/* ЗАДАНИЕ - 3
 * Пользователь вводит 3 числа. Нужно вывести на экран собщение с максимальным числом из введенных
 * */

// const first = +prompt('enter first number')
// const second = +prompt('enter second number')
// const third = +prompt('enter third number')
// variant 1
// if (first > second && first > third) {
//     console.log(first)
// } else if (second > third && second > first) {
//     console.log(second)
// } else {
//     console.log(third)
// }
// variant 2
// let result = Math.max(first, second, third)
// console.log("🚀 ==== > ", result);

/* ЗАДАНИЕ - 4
 * Имитируем список персонала, где у каждого человека есть своя роль. Пользователь вводит имя, после чего нужно вывести сообщение с ролью пользователя.
 * Список следующий:
 * Boss - главный Boss
 * Boss Junior - правая рука Boss'a
 * Gogol Mogol - дeлает Гоголь-Моголь всем сотрудникам
 * John - уборщик
 * */

// const positionPerson = prompt('Кого хочешь узнать')
// example 1
// switch (positionPerson) {
// 	case 'Boss': // positionPerson === 'Boss'
// 		console.log('главный Boss')
// 		break
// 	case 'Boss Junior':
// 		console.log('правая рука Boss"a')
// 		break
// 	case 'Gogol Mogol':
// 		console.log('дeлает Гоголь-Моголь всем сотрудникам')
// 		break
// 	case 'John':
// 		console.log('уборщик')
// 		break
// 	default:
// 		console.log('сотрудник не найден')
// 		break
// }

//  example 2
// if (positionPerson === 'Boss') {
// 	console.log('главный Boss')
// } else if (positionPerson === 'Boss Junior') {
// 	console.log('правая рука Boss"a')
// } else if (positionPerson === 'Gogol Mogol') {
// 	console.log('дeлает Гоголь-Моголь всем сотрудникам')
// } else if (positionPerson === 'John') {
// 	console.log('уборщик')
// } else {
// 	console.log('сотрудник не найден')
// }

/* ЗАДАНИЕ - 5
 * Напишите программу кофейная машина.
 * Программа принимает монеты и готовит напиток (Кофе 25грн, капучино 50грн, чай 10грн).
 * Т.е. пользователь вводит в модальное окно сумму денег и название напитка.
 * В зависимости от того какое название было введено - нужно вычислить сдачу и вывести сообщение: "Ваш напиток *НАЗВАНИЕ НАПИТКА* и сдача *СУММА СДАЧИ*"
 * В случае если пользователь ввел сумму без сдачи - выводить сообщение: "Ваш напиток *НАЗВАНИЕ НАПИТКА*. Спасибо за точную сумму!))"
 * */

let drink = prompt('что хотите выпить')
let money = +prompt('дай денег на напиток')

let change

if (drink === 'coffee') {
	if (money > 25) {
		change = money - 25
		alert(`Ваш напиток ${drink} и сдача ${change}`)
	} else if (money < 25) {
		change = 25 - money
		alert(`Денег не достаточно, вам не хватает - ${change}`)
	} else {
		alert(`Ваш напиток ${drink}. Спасибо за точную сумму!`)
	}
} else if (drink === 'cappuccino') {
    if (money > 50) {
			change = money - 50
			alert(`Ваш напиток ${drink} и сдача ${change}`)
		} else if (money < 50) {
			change = 50 - money
			alert(`Денег не достаточно, вам не хватает - ${change}`)
		} else {
			alert(`Ваш напиток ${drink}. Спасибо за точную сумму!`)
		}
} else if (drink === 'tea') {
    if (money > 10) {
			change = money - 10
			alert(`Ваш напиток ${drink} и сдача ${change}`)
		} else if (money < 10) {
			change = 10 - money
			alert(`Денег не достаточно, вам не хватает - ${change}`)
		} else {
			alert(`Ваш напиток ${drink}. Спасибо за точную сумму!`)
		}
} else {
	console.log('выбраного напитка нема 🤷')
}
