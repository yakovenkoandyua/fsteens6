/* ЗАДАНИЕ - 1
 * Написать функцию, которая принимает 2 аргумента - имя и возраст пользователя
 * Возвращаемое значение этой функции - объект с двумя ключами name и age, куда будут записаны значения переданных функции аргументов.
 * */
// example 1
// function createUser(userName, userAge) {
// 	const userObject = {
// 		name: userName,
// 		age: userAge,
// 	}
// 	return userObject
// }
// example 2
// function createUser(name, age) {
// 	return {
// 		name,
// 		age,
// 	}
// }

// let answerUserName = prompt('What youre name?', 'ivan')
// let answerUserAge = +prompt('What youre age, enter pls?', 55)

// const user = createUser(answerUserName, answerUserAge)
// console.log(user)

// console.log(createUser('vova', 22))

/* ЗАДАНИЕ - 2
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен быть метод, который увеличивает возраст на 1.
 * Т.е. внутри объекта будет свойство\ключ\поле, значением которого будет являться функция,
 * которая увеличивает свойство\ключ\поле age ЭТОГО объекта на 1
 * */

// function createUser(userName, userAge) {
// 	const userObject = {
// 		name: userName,
// 		age: userAge,

//         addAge: function() {
//             userObject.age++
//             // user.age = user.age + 1
//         }
// 	}
// 	return userObject
// }

// const user = createUser('vova', 26)
// console.log(user)
// user.addAge()
// user.addAge()
// console.log(user)

/* ЗАДАНИЕ - 3
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен появиться еще один метод - addField(). Он будет добавлять свойства в объект.
 * Т.е. внутри объекта будет еще одно свойство\ключ\поле, значением которого будет являться функция.
 * Эта функция принимает два аргумента:
 *   1 - имя свойства, которое будет создаваться
 *   2 - значение. которое туда должно быть записано
 * */
// function createUser(userName, userAge) {
// 	const userObject = {
// 		name: userName,
// 		age: userAge,

// 		addAge: function () {
// 			userObject.age++
// 			// userObject.age = userObject.age + 1
// 		},

//         addField: function(key, value) {
//             userObject[key] = value
//         },
// 	}
// 	return userObject
// }

// const user = createUser('vova', 26)
// console.log(user)
// user.addField('hobby', 'basket')
// user.addField('travel', 'paris')
// user.addAge()
// user.addAge()
// console.log(user)

/* ЗАДАНИЕ - 4
 * Написать функцию createProductCart(). Которая создает объект карточки товара.
 *
 * Аргументы:
 *  - title - название товара
 *  - code - артикул товара
 *  - price - цена
 *  - description - описание
 *
 * Возвращаемое значение: объект карточки товара со всеми свойствами и методами
 *
 * Внутри функции нужно создать объект, записать в него све свойства и добавить один метод - startSale(), который изменяет цену товара и делает ее меньше на указанное количество процентов.
 * метод startSale принимает аргумент - размер скидки, одним числом без знака процентов.
 * */

function createProductCart(title, code, price, description) {
	const card = {
		title,
		code,
		price,
		description,

		startSale(sale) {
			let discount = (card.price / 100) * sale
            let newPrice = card.price - discount
            card.price =  newPrice
		},
	}

	return card
}

const iphone = createProductCart('iphone', 13324345, 1000, 'работает так себе')

iphone.startSale(20)


const iphonePro = createProductCart('iphone pro', 133234325, 1500, 'работает так себе, а то и хуже')
console.log("🚀 ==== > iphone", iphonePro === iphone);

// let discount = (product.price / 100) * sale

/* ЗАДАНИЕ - 5
 * Дополнить функционал решения из прошлой задачи.
 * Добавить для свойства price getter и setter.
 * getter должен возвращать своство в виде строки в формате `${ЗНАК ВАЛЮТЫ}${ЦЕНА}`, где знаком валюты должен быть знак гривты.
 * */

/* ЗАДАНИЕ - 6
 * Дополнить функционал решения из прошлой задачи.
 * Заблокировать вохможность изменять свойство code, создаваемого объекта.
 * */
