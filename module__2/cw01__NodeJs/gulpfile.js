import gulp from 'gulp'
import concat from 'gulp-concat'

const buildHtml = () => (
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./prod/'))
)

const buildCss = () => (
    gulp.src('./src/styles/*.css')
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./prod/styles/'))
)

gulp.task('default', gulp.series(buildHtml, buildCss))