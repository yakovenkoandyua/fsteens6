import gulp from 'gulp'
import concat from 'gulp-concat'
import fileinclude from 'gulp-file-include'
import browserSync from 'browser-sync'

browserSync.create()


const path = {
    src: {
        html: './src/index.html',
        css: './src/styles/*.css',
        js: './src/js/*.js',
        img: './src/img/*.*'
    },
    prod: {
        self: './prod/',
        html: './prod/',
        css: './prod/styles/',
        js: './prod/js/',
        img: './prod/img/'
    }
}

const buildHtml = () =>
	gulp
		.src(path.src.html)
		.pipe(
			fileinclude({
				prefix: '@@',
				basepath: '@file',
			}),
		)
		.pipe(gulp.dest(path.prod.html))
        .pipe(browserSync.stream())


const buildCss = () => (
    gulp.src(path.src.css)
        .pipe(concat('style.css'))
        .pipe(gulp.dest(path.prod.css))
        .pipe(browserSync.stream())
)

const watcher = () => {
    browserSync.init({
			server: {
				baseDir: path.prod.html,
			},
		})
    
    gulp.watch('./src/**/*.html', buildHtml).on('change', browserSync.reload)
    gulp.watch(path.src.css, buildCss).on('change', browserSync.reload)
}


gulp.task('default', gulp.series(buildHtml, buildCss, watcher))